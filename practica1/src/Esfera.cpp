//JORGE RAVEN GARCIA 51435 17/10/2016

// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	//aumentando=0;
	radio=0.7f;
	velocidad.x=3;
	velocidad.y=3;
	centro.x=0.0;
	centro.y=0.0;
}


Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro.x=centro.x+velocidad.x*t;
	centro.y=centro.y+velocidad.y*t;
	/*if(!aumentando)
		radio-=0.010;
	else
		radio+=0.010;

	if(radio<=0.05f)
		aumentando=1;
	if(radio>=2.0f)
		aumentando=0;*/
}
