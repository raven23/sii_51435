//JORGE RAVEN GARCIA 51435 17/10/2016

// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <math.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//FIFO SERVIDOR_CLIENTE
	close(fd_servidor_cliente);
	unlink(MYFIFO2);
	//FIFO-HILO
	close(fd_cliente_servidor);
	unlink(FIFOHILO);
	//BOT
	p_Dat_Mem_Comp->Fin=1;
	munmap(Mem_Proyectada,sizeof(Dat_Mem_Comp));
	unlink(DATOS);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	sprintf(cad,"Cliente");
	print(cad,350,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
/*
	esfera.Mueve(0.05f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}
*/

	//FIFO SERVIDOR_CLIENTE
	aux_error=read(fd_servidor_cliente,buff, sizeof(buff));
	if (aux_error==-1){
		unlink(MYFIFO2);
		printf("Error de lectura del FIFO servidor-cliente\n");
		exit(1);
	}
 	sscanf(buff,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 

	//BOT
	p_Dat_Mem_Comp->esfera=esfera;
	p_Dat_Mem_Comp->raqueta1=jugador1;

	if(p_Dat_Mem_Comp->accion==1)
		OnKeyboardDown('w',0,0);
	else if(p_Dat_Mem_Comp->accion==-1)
		OnKeyboardDown('s',0,0);
	else;
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
	case 's':jugador1.velocidad.y=-4;jugador1.Mueve(0.08f);break;
	case 'w':jugador1.velocidad.y=4;jugador1.Mueve(0.08f);break;
	case 'l':jugador2.velocidad.y=-4;jugador2.Mueve(0.08f);break;
	case 'o':jugador2.velocidad.y=4;jugador2.Mueve(0.08f);break;
	}*/
	sprintf(buffhilo,"%c",key);
	write(fd_cliente_servidor,buffhilo,strlen(buffhilo)+1);
}

void CMundo::Init()
{
	//BOT
	FILE=open(DATOS,O_RDWR|O_CREAT|O_TRUNC,0777);
	if (FILE==-1){printf ("\n Error de apertura del fichero donde se quiere proyectar \n");}
	
	write(FILE,&Dat_Mem_Comp,sizeof(Dat_Mem_Comp));

	Mem_Proyectada=(char*)mmap(NULL,sizeof(Dat_Mem_Comp),PROT_WRITE|PROT_READ,MAP_SHARED,FILE,0);
	
	close(FILE);
	
	p_Dat_Mem_Comp=(DatosMemCompartida*)Mem_Proyectada;
	p_Dat_Mem_Comp->Fin=0;
	p_Dat_Mem_Comp->accion=0;
	
	//FIFO SERVIDOR_CLIENTE
	aux_error=mkfifo(MYFIFO2,0666);
	if(aux_error==-1)
	{
		perror("El FIFO Servidor-Cliente no se creo bien");
		unlink(MYFIFO2);
		exit(1);
	}
	//FIFO-HILO
	aux_error=mkfifo(FIFOHILO,0666);
	if(aux_error==-1)
	{
		perror("El FIFO Hilo no se creo bien");
		unlink(FIFOHILO);
		exit(1);
	}
	//S-C
	fd_servidor_cliente=open(MYFIFO2,O_RDONLY);
	if(fd_servidor_cliente==-1)
	{
		perror("El FIFO Servidor-Cliente no se abrio bien");
		exit(-1);
	}
	//HILO
	fd_cliente_servidor=open(FIFOHILO,O_WRONLY);
	if(fd_cliente_servidor==-1)
	{
		perror("El FIFO Hilo no se abrio bien");
		exit(1);
	}

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	
}
