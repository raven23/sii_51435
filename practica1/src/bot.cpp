 //#pragma once

 #include <iostream>
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <sys/mman.h>
 #include <fcntl.h>
 #include <unistd.h>
 #include <stdlib.h>
 #include "DatosMemCompartida.h"
 #define DATOS "myBot"

int main(int argc, char *argv[])
{
	//DatosMemCompartida Dat_Mem_Comp;
	DatosMemCompartida* p_Dat_Mem_Comp;
	char* Mem_Proyectada;
	int FILE;
	
	FILE=open(DATOS,O_RDWR);
	if (FILE==-1){
		perror("\n Error de apertura del fichero en bot \n");
		exit(1);
		}
	Mem_Proyectada=(char*)mmap(NULL,sizeof(*p_Dat_Mem_Comp),PROT_WRITE|PROT_READ,MAP_SHARED,FILE,0);
	if (Mem_Proyectada==MAP_FAILED){
		perror("Error en la proyeccion del fichero");
		close(FILE);
		exit(1);
		}
	int aux_error=close(FILE);
	if(aux_error==-1) perror("Error en el close");
	p_Dat_Mem_Comp=(DatosMemCompartida*)Mem_Proyectada;
	p_Dat_Mem_Comp->Fin=0;
	p_Dat_Mem_Comp->accion=0;
	while(1)
	{
		float centro_raqueta=(p_Dat_Mem_Comp->raqueta1.y1+p_Dat_Mem_Comp->raqueta1.y2)/2;

		if(centro_raqueta<p_Dat_Mem_Comp->esfera.centro.y)
			p_Dat_Mem_Comp->accion=1;
		else if(centro_raqueta>p_Dat_Mem_Comp->esfera.centro.y)
			p_Dat_Mem_Comp->accion=-1;
		else
			p_Dat_Mem_Comp->accion=0;
		if(p_Dat_Mem_Comp->Fin==1)
			break;
		usleep(25000);
	}
	munmap(Mem_Proyectada,sizeof(*p_Dat_Mem_Comp));
	unlink(DATOS);
	return 0;
	

}
