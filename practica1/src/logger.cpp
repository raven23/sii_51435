
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <string.h>

#define MAX 200

#define MYFIFO "my_FIFO"

int main(int argc, char *argv[])
{
	int fd;
	char buffer[MAX];
	int aux_error;

	aux_error=mkfifo(MYFIFO,0666);
	if(aux_error==-1)
	{
		perror("El FIFO no se creo bien");
		unlink(MYFIFO);
		exit(-1);
	}

	fd=open(MYFIFO,O_RDONLY);
	if(fd==-1)
	{
		perror("El FIFO no se abrio bien");
		exit(-1);
	}
	printf("\nInicio del juego:\nJugador 1:0 - Jugador 2:0\n\n");


 while(1){
	aux_error=read(fd,buffer, sizeof(buffer));
	if (aux_error==-1){
		unlink(MYFIFO);
		printf("Error de lectura del FIFO\n");
		exit(-1);
	}
	else if(buffer[0]=='F'){
		printf("%s\n", buffer);
		break;
		}
	else
 		printf("%s\n", buffer);	

 }

	
	close(fd);
	unlink(MYFIFO);
	exit(0);
}
