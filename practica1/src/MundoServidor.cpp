//JORGE RAVEN GARCIA 51435 17/10/2016

// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <math.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void* hilo_comandos(void* d);


SMundo::SMundo()
{
	Init();
}

SMundo::~SMundo()
{
	//FIFO
	sprintf(buffer,"Fin del juego");
	write(fd_logger,buffer,strlen(buffer)+1);
	close(fd_logger);
	unlink(MYFIFO);
	//FIFO SERVIDOR_CLIENTE
	close(fd_servidor_cliente);
	unlink(MYFIFO2);
	//FIFO-HILO
	close(fd_cliente_servidor);
	unlink(FIFOHILO);
	
}

void SMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void SMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	sprintf(cad,"Servidor");
	print(cad,350,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void SMundo::OnTimer(int value)
{	
//	jugador1.Mueve(0.1f);
//	jugador2.Mueve(0.1f);
	esfera.Mueve(0.05f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		sprintf(buffer,"Jugador 2 se anota un tanto\nJugador 1: %d - Jugador 2: %d \n",puntos1,puntos2);
		//Escribir en el FIFO
		write(fd_logger,buffer,strlen(buffer)+1);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		sprintf(buffer,"Jugador 1 se anota un tanto\nJugador 1: %d - Jugador 2: %d \n",puntos1,puntos2);
		//Escribir en el FIFO
		write(fd_logger,buffer,strlen(buffer)+1);
	}
	//FIFO SERVIDOR_CLIENTE
	sprintf(buff,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	write(fd_servidor_cliente,buff,strlen(buff)+1);

	if(puntos1==3){
		printf("\n\n--GANA EL JUGADOR 1--\n\n");
		exit(0);}
	if(puntos2==3){
		printf("\n\n--GANA EL JUGADOR 2--\n\n");
		exit(0);}
}

void SMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
	case 's':jugador1.velocidad.y=-4;jugador1.Mueve(0.08f);break;
	case 'w':jugador1.velocidad.y=4;jugador1.Mueve(0.08f);break;
	case 'l':jugador2.velocidad.y=-4;jugador2.Mueve(0.08f);break;
	case 'o':jugador2.velocidad.y=4;jugador2.Mueve(0.08f);break;
	}*/
}

void SMundo::Init()
{
	//Inicio FIFO
	fd_logger=open(MYFIFO,O_WRONLY);
	if(fd_logger==-1)
	{
		perror("Error al abrir el FIFO");
	}
	//FIFO SERVIDOR-CLIENTE
	fd_servidor_cliente=open(MYFIFO2,O_WRONLY);
	if(fd_servidor_cliente==-1)
	{
		perror("Error al abrir el FIFO servidor-cliente");
	}


	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//THREAD
	pthread_create(&thread, NULL, hilo_comandos, this);
}

void* hilo_comandos(void* d)
{
        SMundo* p=(SMundo*) d;
	//FIFO Hilo
	p->fd_cliente_servidor=open(FIFOHILO,O_RDONLY);
	if(p->fd_cliente_servidor==-1)
	{
		perror("Error al abrir el FIFO Hilo-cliente");
	}
        p->RecibeComandosJugador();
	pthread_exit(0);
}

void SMundo::RecibeComandosJugador()
{
	
     while (1) {
            usleep(10);
            char cad[MAXIMO];
            read(fd_cliente_servidor, cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            switch(key)
		{
			case 's':jugador1.velocidad.y=-4;jugador1.Mueve(0.08f);break;
			case 'w':jugador1.velocidad.y=4;jugador1.Mueve(0.08f);break;
			case 'l':jugador2.velocidad.y=-4;jugador2.Mueve(0.08f);break;
			case 'o':jugador2.velocidad.y=4;jugador2.Mueve(0.08f);break;
		}
      }

}
