// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <ctype.h>
#include <string.h>

#include "DatosMemCompartida.h"

#define DATOS "myBot"
#define MYFIFO2 "Servidor-Cliente"
#define FIFOHILO "FIFO-HILO"
#define MAX 200
#define MAXIMO 100

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	//BOT
	DatosMemCompartida Dat_Mem_Comp;
	DatosMemCompartida* p_Dat_Mem_Comp;
	int FILE;
	char* Mem_Proyectada;

	//FIFO
	int fd_servidor_cliente,fd_cliente_servidor;
	char buff[MAX];
	char buffhilo[MAXIMO];
	int aux_error;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
